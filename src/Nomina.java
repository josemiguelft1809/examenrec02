/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class Nomina {
    private int numRegistro;
    private String nombre;
    private int puesto;
    private int nivel;
    private int dias;
    
    public Nomina(){
        this.numRegistro = 0;
        this.nombre= "" ;
        this.puesto=0;
        this.nivel=0;
        this.dias=0;
    }
    public Nomina(int numRegistro, String nombre, int puesto, int nivel, int dias) {
        this.numRegistro = numRegistro;
        this.nombre = nombre;
        this.puesto = this.puesto;
        this.nivel = nivel;
        this.dias = dias;
    }
    public Nomina(Nomina x){
        this.numRegistro=x.numRegistro;
        this.nombre=x.nombre;
        this.puesto=x.puesto;
        this.nivel=x.nivel;
        this.dias=x.dias;
    }

    public int getNumRegistro() {
        return numRegistro;
    }

    public void setNumRegistro(int numRegistro) {
        this.numRegistro = numRegistro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    public float calcularPago(){
        float pago = 0.0f;
        if(this.puesto==0){
            pago= 100;
        }
        if(this.puesto==1){
            pago=200;
        }
        if(this.puesto==2){
            pago=300;
        }
        pago= pago *this.dias;
        return pago;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        if(this.nivel==0){
            impuesto= (float) (this.calcularPago()*0.05);
        }
        if(this.nivel==1){
            impuesto= (float) (this.calcularPago()*0.03);
        }
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total= 0.0f;
        total= this.calcularPago()-this.calcularImpuesto();
        return total;
    }
    
}
